FROM gradle:8.3 as java-build

WORKDIR /project

COPY build.gradle.kts settings.gradle.kts /project/
COPY src /project/src/

RUN gradle jar
RUN cp build/libs/armeria-hello-world.jar /opt/armeria-hello-world.jar

FROM ghcr.io/graalvm/native-image-community:21 as native-build

WORKDIR /opt
COPY --from=java-build /opt/armeria-hello-world.jar /opt/armeria-hello-world.jar

RUN native-image -jar /opt/armeria-hello-world.jar --static

FROM gcr.io/distroless/base-nossl:nonroot

COPY --from=native-build /opt/armeria-hello-world /opt/armeria-hello-world

ENV APP_PORT 6666

ENTRYPOINT ["/opt/armeria-hello-world"]
