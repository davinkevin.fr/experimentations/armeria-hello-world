plugins {
    kotlin("jvm") version "1.9.10"
    application
}

repositories {
    mavenCentral()
}

dependencies {
    implementation("com.linecorp.armeria:armeria:1.25.2")
    implementation("com.linecorp.armeria:armeria-kotlin:1.25.2")
    implementation("ch.qos.logback:logback-classic:1.4.11")
    implementation("org.slf4j:log4j-over-slf4j:1.7.36")
}

application {
    mainClass = "io.gitlab.davinkevin.armeria.helloworld.MainKt"
}

tasks.jar {
    manifest { attributes["Main-Class"] = "io.gitlab.davinkevin.armeria.helloworld.MainKt" }
    val dependencies = configurations
        .runtimeClasspath
        .get()
        .map(::zipTree)
    from(dependencies)
    duplicatesStrategy = DuplicatesStrategy.EXCLUDE
}
