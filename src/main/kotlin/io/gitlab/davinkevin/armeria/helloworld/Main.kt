package io.gitlab.davinkevin.armeria.helloworld

import com.linecorp.armeria.common.HttpRequest
import com.linecorp.armeria.common.HttpResponse
import com.linecorp.armeria.common.stream.StreamMessage
import com.linecorp.armeria.server.Server
import com.linecorp.armeria.server.ServiceRequestContext
import java.nio.file.Files
import java.nio.file.Path
import java.util.logging.Logger
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream
import kotlin.io.path.isRegularFile
import kotlin.io.path.name

private val logger = Logger.getLogger("Main")
fun main() {

    val port = Result.runCatching { System.getenv("APP_PORT").toInt() }.getOrElse { 7777 }

    val server = Server.builder().apply {
        http(port)
        service("/") { _, _ -> HttpResponse.of("Hello, Armeria!") }
        service("/download", ::serveFile)
    }
        .build()

    server.closeOnJvmShutdown()
    server.start().join()

    logger.info("Server has been started. Serving dummy service at http://127.0.0.1:${server.activeLocalPort()}")
}

fun serveFile(ctx: ServiceRequestContext, req: HttpRequest): HttpResponse {
    val folder = Path.of(System.getenv("APP_FOLDER") ?: "/opt")
    logger.info("getting file from $folder")
    val body = StreamMessage.fromOutputStream { os ->
        val oz = os.buffered().let(::ZipOutputStream)

        Files.walk(folder)
            .filter { it.isRegularFile() }
            .forEach {
                logger.info("add entry: ${it.name}")
                oz.putNextEntry(ZipEntry(it.name))
                Files.copy(it, oz)
                oz.closeEntry()
            }

        oz.close()
    }

    return HttpResponse.builder()
        .ok()
        .content(body)
        .build()
}
